package agence.colonie.capteur;

import gestionnaire.Environnement;

public class DetecteurMinerai {
    public DetecteurMinerai() {

    }

    /**
     * 1 Minerai dispo
     * 0 Marqueur présent sur le minerai
     *
     *
     * -1 pas de nourriture
     * @param env
     * @param posx
     * @param posy
     * @return
     */
    public int detection(Environnement env, int posx, int posy) {
        if (env.getCaseCourante(posx,posy) == 'm') {
            if (env.getMarqueur(posx,posy)) return 0;
            else return 1;
        } else {
            return -1;
        }
    }
}
