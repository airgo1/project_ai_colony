package agence.colonie.capteur;

import gestionnaire.Environnement;
import gestionnaire.InfoGame;

import java.util.Map;

/**
 * Le detecteur d'eau détecte les liquides à une distance de 2 cases du robot :
 */

public class DetecteurEau {

    public DetecteurEau() {

    }

    public Boolean caseEauAdjacent(Environnement env, int posx, int posy) {
        Map<String, String> map = InfoGame.getOrientation();
        // N NE E SE S SW W NW
        // Verification du nord
        for (int i = 0; i < 8; i++) {
            if (verif(getPosition(map.get(Integer.toString(i))), env, posx, posy)) {
                return true;
            }
        }
        return false;
    }

    public Boolean detectePipeline(Environnement env, int posx, int posy) {
        if (env.verifPipeline(posx, posy) == 1) return true;
        else return false;
    }

    /**
     * Permet de vérifier que c'est bien un obstacle infranchissable
     *
     * @param pos
     * @param env  Lien avec l'environnement
     * @param posx
     * @param posy
     * @return
     */
    private Boolean verif(int[] pos, Environnement env, int posx, int posy) {
        if ((posx + pos[0] - 1) >= 0 || (posx + pos[0] - 1) < env.getSizeX() || (posy + pos[1] - 1) >= 0 || (posy + pos[1] - 1) < env.getSizeY()) {
            if (env.getCaseCourante(posx + pos[0] -1, posy + pos[1] -1) == 'e') return true;
            else return false;
        } else {
            return false;
        }
    }

    private int[] getPosition(String str) {
        int[] pos = {Integer.parseInt(str.split("-")[0]), Integer.parseInt(str.split("-")[1])};
        return pos;
    }

}
