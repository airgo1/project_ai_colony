package agence.colonie.capteur;

import gestionnaire.Environnement;
import gestionnaire.InfoGame;

import java.util.Map;

public class DetecteurNourriture {
    public DetecteurNourriture() {
    }

    /**
     * 1 Nourriture dispo
     * 0 Marqueur présent sur la nourriture
     * -1 pas de nourriture
     * @param env
     * @param posx
     * @param posy
     * @return
     */
    public int detection(Environnement env, int posx, int posy) {
        if (env.getCaseCourante(posx,posy) == 'o') {
            if (env.getMarqueur(posx,posy)) return 0;
            else return 1;
        } else {
            return -1;
        }
    }

}
