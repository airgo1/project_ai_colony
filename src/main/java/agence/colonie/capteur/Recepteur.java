package agence.colonie.capteur;

import java.util.LinkedList;

/**
 * Permet à une entité de recevoir des informations d'un emetteur
 * On sauvegarde les messages non-lu par le centralisateur
 *
 */
public class Recepteur {

    LinkedList<String> listeMessage;

    public Recepteur(){
        listeMessage = new LinkedList<>();
    }

    public int getNbMessage()
    {
        return listeMessage.size();
    }

    public String getLastMessage()
    {
        return listeMessage.removeLast();
    }

    public void addMessage(String message)
    {
        listeMessage.add(message);
    }
}
