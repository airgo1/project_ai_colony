package agence.colonie.capteur;

/**
 * Permet aux robots de données des informations au centralisateur
 */
public class Emetteur {
    private Recepteur recepteur;


    public Emetteur(Recepteur recepteur)
    {
        this.recepteur = recepteur;
    }

    public void sendMessage(String message)
    {
        recepteur.addMessage(message);
    }

}
