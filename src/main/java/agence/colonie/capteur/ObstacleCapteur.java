package agence.colonie.capteur;

import gestionnaire.Environnement;
import gestionnaire.InfoGame;

import java.util.Map;

/**
 * Classe permettant de détecter les obstacles autour du capteur
 */
public class ObstacleCapteur {

    public ObstacleCapteur() {
    }

    private int[] getPosition(String str) {
        int[] pos = {Integer.parseInt(str.split("-")[0]), Integer.parseInt(str.split("-")[1])};
        return pos;
    }

    /**
     * Permet de récupérer l'environnement autour du capteur
     *
     * @param posx
     * @param posy
     * @return
     */

    public String detection(Environnement env, int posx, int posy) {

        Map<String, String> map = InfoGame.getOrientation();

        char[] donnee = "0000000000000000".toCharArray();
        // N NE E SE S SW W NW
        // Verification du nord

        donnee[8] = verif(getPosition(map.get("0")), env, posx, posy);
        donnee[9] = verif(getPosition(map.get("1")), env, posx, posy);
        donnee[10] = verif(getPosition(map.get("2")), env, posx, posy);
        donnee[11] = verif(getPosition(map.get("3")), env, posx, posy);
        donnee[12] = verif(getPosition(map.get("4")), env, posx, posy);
        donnee[13] = verif(getPosition(map.get("5")), env, posx, posy);
        donnee[14] = verif(getPosition(map.get("6")), env, posx, posy);
        donnee[15] = verif(getPosition(map.get("7")), env, posx, posy);

        return new String(donnee);

    }

    /**
     * Permet de vérifier que c'est bien un obstacle infranchissable
     *
     * @param pos
     * @param env Lien avec l'environnement
     * @param posx
     * @param posy
     * @return
     */
    private char verif(int[] pos, Environnement env, int posx, int posy) {
        if ((posx + pos[0]-1) >= 0 || (posx + pos[0]-1) < env.getSizeX() || (posy + pos[1]-1) >= 0 || (posy + pos[1]-1) < env.getSizeY()) {
            if (env.getObstable(posx, posy, pos[0], pos[1], 1)) return '1';
            else return '0';
        } else {
            return '0';
        }
    }


}
