package agence.colonie;

import agence.colonie.capteur.DetecteurEau;
import agence.colonie.capteur.DetecteurNourriture;
import agence.colonie.capteur.Recepteur;
import gestionnaire.Environnement;

/**
 * L'agent ouvrier doit rechercher un point d'eau pour construire un pipeline
 */
public class OuvrierPipelineAgent extends Agent {

    // Si l'ouvrier est en train de construire il n'est plus en recherche ni en inspection
    // Ces trois valeurs détermine l'action que va effectuer le robot sur le terrain
    private Boolean construction;
    private Boolean recherche;
    private Boolean inspection;
    private int nbTourConstruction;
    private DetecteurEau detecteurEau;

    private Boolean retourBase;

    private int posInitialPipelineX;
    private int posInitialPipelineY;

    public OuvrierPipelineAgent(int posx, int posy, Recepteur recepteur) {
        super(posx, posy, recepteur);
        construction = false;
        recherche = true;
        inspection = false;
        nbTourConstruction = 2;

        detecteurEau = new DetecteurEau();
    }

    /**
     * Permet de gérer le tour des ouvrier qui gére les pipelines
     *
     * @param env
     * @return
     */
    public int tour(Environnement env) {
        // L'ouvrier se déplace aléatoirement s'il est en mode recherche
        if (recherche.equals(true)) {
            //La méthode permet de faire un déplacement aléatoire en fonction des informations reçu par le capteur d'obstacle
            choixDeplacement(env);

            if (detecteurEau.caseEauAdjacent(env, getPosx(), getPosy())) {
                construction = true;
                recherche = false;
                posInitialPipelineX = getPosx();
                posInitialPipelineY = getPosy();
                // Envoie au cartographe la position du pipeline initial
                sendMessage("pipeline-" + getPosx() + "-" + getBasePosY());
            }
        } else if (construction.equals(true)) {
            if (nbTourConstruction != 0) {
                nbTourConstruction--;
            } else {
                env.construirePipeline(getPosx(), getPosy());
                // Retour à la base grâce à un algorithme de recherche
                Boolean resultat = allerAstar(env);
                nbTourConstruction = 2;

                if ((getPosx() == getBasePosX() && getPosy() == getBasePosY()) || detecteurEau.detectePipeline(env, getPosx(), getPosy())) {
                    construction = false;
                    inspection = true;
                }
            }
        } else if (inspection.equals(true)) {
            // L'inspection consiste à suivre le pipeline pour vérifier son intégrité physique suite à une métamorphose de la planète
            System.out.println("inspection ...");
        }
        // sendMessage("");
        return 0;
    }
}
