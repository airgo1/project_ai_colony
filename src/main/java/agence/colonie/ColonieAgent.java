package agence.colonie;

import gestionnaire.Environnement;

import java.util.ArrayList;

/**
 * N'est pas vraiment un Agent mais plus un gestionnaire d'Agent pour gérer les tours
 */
public class ColonieAgent {

    private int posx;
    private int posy;

    // Agent immobile chargé de concevoir la map au fur et à mesure de l'avancement des robots.
    private CartographeAgent cartographe;
    private ArrayList<Agent> listeRobot;

    private Integer RessourceEau = 0;
    private Integer RessourceMinerai = 0;
    private Integer RessourceNourriture = 0;
    private int nourriture;
    private int minerai;
    private int eau;

    private int extraction;
    private int prelevement;

    public ColonieAgent(int posx, int posy) {
        this.posx = posx;
        this.posy = posy;
        cartographe = new CartographeAgent();

        listeRobot = new ArrayList<>();
        //listeRobot.add(new AgriculteurAgent(posx, posy, cartographe.getRecepteur()));
        //listeRobot.add(new AgriculteurAgent(posx, posy, cartographe.getRecepteur()));

        for (int i = 0; i < 3; i++) {
            //listeRobot.add(new OuvrierNourritureAgent(posx, posy, cartographe.getRecepteur()));
            //listeRobot.add(new OuvrierPipelineAgent(posx, posy, cartographe.getRecepteur()));
            listeRobot.add(new OuvrierMineraiAgent(posx, posy, cartographe.getRecepteur()));

        }

    }

    /**
     * Permet la gestion des tours de chaque agents du système
     *
     * @return
     */
    public int lancementTour(Environnement environnement) {
        extraction = 0;
        prelevement = 0;
        cartographe.tour();
        for (Agent e : listeRobot) {
            int resultat = e.tour(environnement);
            if (resultat > 0) {
                if (e.getNom().equals("recolteurNourriture")) {
                    System.out.println("Nourriture Déposé : passe de " + nourriture + " à " + (nourriture + 10));
                    nourriture = nourriture + 10;
                } else if (e.getNom().equals("recolteurMinerai")) {
                    System.out.println("Minerai Déposé : passe de " + minerai + " à " + (minerai + 2));
                    minerai = minerai + 2;
                    extraction = extraction + 2;
                }
            }

        }
        return 0;
    }

    public int getPosx() {
        return posx;
    }

    public int getPosy() {
        return posy;
    }

    public int nbRobot() {
        return listeRobot.size();
    }

    public CartographeAgent getCartographe() {
        return cartographe;
    }

    public ArrayList<Agent> getListeRobot() {
        return listeRobot;
    }

    public Integer getRessourceEau() {
        return RessourceEau;
    }

    public void setRessourceEau(Integer ressourceEau) {
        RessourceEau = ressourceEau;
    }

    public Integer getRessourceMinerai() {
        return RessourceMinerai;
    }

    public void setRessourceMinerai(Integer ressourceMinerai) {
        RessourceMinerai = ressourceMinerai;
    }

    public Integer getRessourceNourriture() {
        return RessourceNourriture;
    }

    public void setRessourceNourriture(Integer ressourceNourriture) {
        RessourceNourriture = ressourceNourriture;
    }
    public int getNouveauPrelevement() {
        return prelevement;
    }
    public int getNouvelleExtraction() {
        return extraction;
    }
}
