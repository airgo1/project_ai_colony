package agence.colonie;

import agence.colonie.capteur.Recepteur;

import java.util.ArrayList;

/**
 * L'agent cartographe ne se déplace pas.
 * Il est censé récolter les informations renvoyé par les robots en tout temps.
 * On peut supposer qu'il partage ça mémoire avec une IA stratège qui pourra donner des informations aux robots sur le terrain
 * pour les rendre plus efficace sans pour autant leur supprimer leur autonomie
 */
public class CartographeAgent {
    // Il ne connait pas forcément la taille de la planète
    // Il doit recevoir un compte rendu de chaque robot sur leurs activités

    private ArrayList<Agent> listeRobot;
    private Recepteur recepteur;

    public CartographeAgent() {
        listeRobot = new ArrayList<>();
        recepteur = new Recepteur();
    }

    /**
     * Permet de récupérer le récepteur du cartographe pour permettre aux ouvriers de lui envoyer un message
     * par le biais de leur emetteur
     * @return Recepteur
     */
    public Recepteur getRecepteur() {
        return recepteur;
    }

    /**
     * Partie de l'IA de l'agent
     *       - Comment il traite les données qui lui sont envoyer
     *       - Cartographie de la planete grâce aux données etc...
     */
    public void tour() {
        if (recepteur.getNbMessage() > 0) {
            System.out.println(recepteur.getLastMessage());
        }
    }


}
