package agence.colonie;

import agence.colonie.capteur.Emetteur;
import agence.colonie.capteur.ObstacleCapteur;
import agence.colonie.capteur.Recepteur;
import gestionnaire.Environnement;
import gestionnaire.InfoGame;
import gestionnaire.utils.Astar;

public abstract class Agent {

    private int posx;
    private int posy;
    private int etat;

    private String nom;
    private int basePosX;
    private int basePosY;

    // Tous les robots ont un capteur de proximité avec eux
    private ObstacleCapteur capteur;
    private Emetteur emetteur;

    public Agent(int posx, int posy, Recepteur recepteur) {
        this.etat = 100;
        this.posx = posx;
        this.posy = posy;
        capteur = new ObstacleCapteur();
        emetteur = new Emetteur(recepteur);

        this.basePosX = posx;
        this.basePosY = posy;
    }

    public void sendMessage(String message) {
        emetteur.sendMessage(message);
    }

    public abstract int tour(Environnement environment);

    public void choixDeplacement(Environnement environnement) {
        String infoCapteur = capteur.detection(environnement, posx, posy);
        char[] c = infoCapteur.toCharArray();
        // La partie intéressante de la matrice commence à l'index 8
        Boolean deplacement = false;
        int choix = 0;

        while (deplacement == false) {
            choix = (int) (Math.random() * 8);
            if (c[choix + 8] == '0') deplacement = true;
        }

        // on a juste à choisir un des index du tableau de char pour savoir dans quelle direction on peut aller
        String[] direction = InfoGame.getOrientation().get(Integer.toString(choix)).split("-");
        posx = posx + Integer.parseInt(direction[0]) - 1;
        posy = posy + Integer.parseInt(direction[1]) - 1;

    }

    public Boolean allerAstar(Environnement env) {

        Astar astar = new Astar(env, posx, posy, basePosX, basePosY);

        //Choix du déplacement:

        if (posx > 0) {
            if (astar.getPos()[posx - 1][posy] == 8) {
                posx = posx - 1;
                return true;
            } else if (posy > 0 && astar.getPos()[posx - 1][posy - 1] == 8) {
                posx = posx -1;
                posy = posy -1;
                return true;
            } else if (posy < env.getSizeY() - 1 && astar.getPos()[posx - 1][posy + 1] == 8) {
                posx = posx -1;
                posy =posy +1;
                return true;
            }
        }
        if (posx < env.getSizeX() - 1) {
            if (astar.getPos()[posx + 1][posy] == 8) {
                posx = posx + 1;
                return true;
            } else if (posy > 0 && astar.getPos()[posx + 1][posy - 1] == 8) {
                posx = posx +1;
                posy =posy -1;
                return true;
            } else if (posy < env.getSizeY() - 1 && astar.getPos()[posx + 1][posy + 1] == 8) {
                posx = posx +1;
                posy =posy +1;
                return true;
            }
        }
        if (posy > 0 && astar.getPos()[posx][posy - 1] == 8) {
            posy = posy - 1;
            return true;
        } else if (posy < env.getSizeY() - 1 && astar.getPos()[posx][posy + 1] == 8) {
            posy = posy + 1;
            return true;
        }
        return false;
    }

    public Boolean allerAstar(Environnement env, int destPosX, int destPosY) {

        Astar astar = new Astar(env, posx, posy, destPosX, destPosY);

        //Choix du déplacement:
        if (posx > 0) {
            if (astar.getPos()[posx - 1][posy] == 8) {
                posx = posx - 1;
                return true;
            } else if (posy > 0 && astar.getPos()[posx - 1][posy - 1] == 8) {
                posx = posx -1;
                posy = posy -1;
                return true;
            } else if (posy < env.getSizeY() - 1 && astar.getPos()[posx - 1][posy + 1] == 8) {
                posx = posx -1;
                posy =posy +1;
                return true;
            }
        }
        if (posx < env.getSizeX() - 1) {
            if (astar.getPos()[posx + 1][posy] == 8) {
                posx = posx + 1;
                return true;
            } else if (posy > 0 && astar.getPos()[posx + 1][posy - 1] == 8) {
                posx = posx +1;
                posy =posy -1;
                return true;
            } else if (posy < env.getSizeY() - 1 && astar.getPos()[posx + 1][posy + 1] == 8) {
                posx = posx +1;
                posy =posy +1;
                return true;
            }
        }
        if (posy > 0 && astar.getPos()[posx][posy - 1] == 8) {
            posy = posy - 1;
            return true;
        } else if (posy < env.getSizeY() - 1 && astar.getPos()[posx][posy + 1] == 8) {
            posy = posy + 1;
            return true;
        }
        return false;
    }

    public int getPosx() {
        return posx;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public int getPosy() {
        return posy;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public ObstacleCapteur getCapteur() {
        return capteur;
    }

    public void setCapteur(ObstacleCapteur capteur) {
        this.capteur = capteur;
    }

    public Emetteur getEmetteur() {
        return emetteur;
    }

    public void setEmetteur(Emetteur emetteur) {
        this.emetteur = emetteur;
    }

    public int getBasePosX() {
        return basePosX;
    }

    public void setBasePosX(int basePosX) {
        this.basePosX = basePosX;
    }

    public int getBasePosY() {
        return basePosY;
    }

    public void setBasePosY(int basePosY) {
        this.basePosY = basePosY;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
