package agence.colonie;

import agence.colonie.capteur.DetecteurMinerai;
import agence.colonie.capteur.Recepteur;
import gestionnaire.Environnement;

public class OuvrierMineraiAgent extends Agent {

    private Boolean transporte;
    private DetecteurMinerai detecteurMinerai;

    private Boolean marqueurPose;
    private int posxMarqueur;
    private int posyMarqueur;

    public OuvrierMineraiAgent(int posx, int posy, Recepteur recepteur) {
        super(posx, posy, recepteur);
        transporte = false;
        marqueurPose = false;
        detecteurMinerai = new DetecteurMinerai();
        setNom("recolteurMinerai");
    }

    /**
     * On est sûr qu'il y a du minerai
     *
     * @param env
     */
    private Boolean prendreMinerai(Environnement env) {
        if (env.getMarqueur(getPosx(), getPosy())) {
            if (marqueurPose && posxMarqueur == getPosx() && posyMarqueur == getPosy()) {
                transporte = true;
                if (env.recolteMinerai(getPosx(), getPosy()) == 1) {
                    marqueurPose = false;
                }
                return true;
            } else {
                return false;
            }
        } else {
            env.addMarqueur(getPosx(), getPosy());
            posyMarqueur = getPosy();
            posxMarqueur = getPosx();
            transporte = true;
            if (env.recolteMinerai(getPosx(), getPosy()) == 0) {
                marqueurPose = true;
            }
            return true;
        }
    }

    public int tour(Environnement env) {

        int deposer = 0;
        // L'ouvrier se déplace aléatoirement s'il n'a pas de nourriture
        int resultatDetecteur = detecteurMinerai.detection(env, getPosx(), getPosy());

        // L'ouvrier se déplace aléatoirement s'il n'a pas de nourriture
        if ((resultatDetecteur == 1 || (resultatDetecteur == 0 && posxMarqueur == getPosx() && posyMarqueur == getPosy())) && !transporte) {
            Boolean minerai = prendreMinerai(env);
        } else if (transporte.equals(true)) {
            // retour à la base
            Boolean resultAstar = allerAstar(env);
            if (resultAstar && getPosy() == getBasePosY() && getPosx() == getBasePosX()) {
                transporte = false;
                deposer = 1;
            }
        } else {
            //La méthode permet de faire un déplacement aléatoire en fonction des informations reçu par le capteur d'obstacle
            // La méthode fait également le changement de position
            if (marqueurPose) {
                allerAstar(env, posxMarqueur, posyMarqueur);
                if (getPosx() == posxMarqueur && posyMarqueur == getPosy() && detecteurMinerai.detection(env, getPosx(), getPosy()) == -1) {
                    marqueurPose = false;
                }
            } else {
                //La méthode permet de faire un déplacement aléatoire en fonction des informations reçu par le capteur d'obstacle
                // La méthode fait également le changement de position
                choixDeplacement(env);
            }
        }
        // sendMessage("");
        return deposer;
    }

}
