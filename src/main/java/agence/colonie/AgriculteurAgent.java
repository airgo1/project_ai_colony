package agence.colonie;

import agence.colonie.capteur.Recepteur;
import gestionnaire.Environnement;

class AgriculteurAgent extends Agent{


    // Si l'ouvrier est en train de construire il n'est plus en recherche ni en inspection
    // Ces trois valeurs détermine l'action que va effectuer le robot sur le terrain
    private Boolean construction    = false;
    private Boolean recherche     = true;
    private Boolean inspection      = false;

    private int InspecLastPositionX;
    private int InspecLastPositionY;

    private int tourConstruction;
    public AgriculteurAgent(int posx, int posy, Recepteur recepteur) {

        super(posx, posy, recepteur);
        construction = false;
        recherche = true;
        inspection = false;
        tourConstruction = 2;
    }

    public int tour(Environnement env) {
        // L'ouvrier se déplace aléatoirement s'il est en mode recherche
        if (recherche.equals(true)) {
            //La méthode permet de faire un déplacement aléatoire en fonction des informations reçu par le capteur d'obstacle
            choixDeplacement(env);
        } else if (construction.equals(true)) {
            // Retour à la base grâce à un algorithme de recherche
            allerAstar(env, getBasePosX(), getBasePosY());

        } else if (inspection.equals(true)) {
            // L'inspection consiste à suivre le pipeline pour vérifier son intégrité physique suite à une métamorphose de la planète
            int pipeline = env.verifPipeline(getPosx(),getPosy());

        }
        // sendMessage("");
        return 0;
    }
}
