package agence.colonie;

import agence.colonie.capteur.DetecteurNourriture;
import agence.colonie.capteur.Recepteur;
import gestionnaire.Environnement;

public class OuvrierNourritureAgent extends Agent {

    private Boolean transporte;
    private DetecteurNourriture detecteurNourriture;

    private Boolean marqueurPose;
    private int posxMarqueur;
    private int posyMarqueur;

    public OuvrierNourritureAgent(int posx, int posy, Recepteur recepteur) {
        super(posx, posy, recepteur);
        transporte = false;
        marqueurPose = false;
        detecteurNourriture = new DetecteurNourriture();
        setNom("recolteurNourriture");
    }

    /**
     * On est sûr qu'il y a de la nourriture
     *
     * @param env
     */
    private Boolean prendreNourriture(Environnement env) {
        if (env.getMarqueur(getPosx(), getPosy())) {
            if (marqueurPose && posxMarqueur == getPosx() && posyMarqueur == getPosy()) {
                transporte = true;
                if (env.recolteNourriture(getPosx(), getPosy()) == 1) {
                    marqueurPose = false;
                }
                return true;
            } else {
                return false;
            }
        } else {
            env.addMarqueur(getPosx(), getPosy());
            posyMarqueur = getPosy();
            posxMarqueur = getPosx();
            transporte = true;
            if (env.recolteNourriture(getPosx(), getPosy()) == 0) {
                marqueurPose = true;
            }
            return true;
        }
    }

    /**
     * Permet de gérer le tour complet d'un récolteur de nourriture
     * @param env
     * @return
     */
    public int tour(Environnement env) {
        int deposer = 0;
        // L'ouvrier se déplace aléatoirement s'il n'a pas de nourriture
        int resultatDetecteur = detecteurNourriture.detection(env, getPosx(), getPosy());

        if ((resultatDetecteur == 1 || (resultatDetecteur == 0 && posxMarqueur == getPosx() && posyMarqueur == getPosy())) && !transporte) {
            Boolean nourriture = prendreNourriture(env);
        } else if (transporte) {
            // retour à la base
            Boolean resultAstar = allerAstar(env);
            if (resultAstar && getPosy() == getBasePosY() && getPosx() == getBasePosX()) {
                transporte = false;
                deposer = 1;
            }
        } else {
            if (marqueurPose) {
                allerAstar(env, posxMarqueur, posyMarqueur);
                if (getPosx() == posxMarqueur && posyMarqueur == getPosy() && detecteurNourriture.detection(env, getPosx(), getPosy()) == -1) {
                    marqueurPose = false;
                }
            } else {
                //La méthode permet de faire un déplacement aléatoire en fonction des informations reçu par le capteur d'obstacle
                // La méthode fait également le changement de position
                choixDeplacement(env);
            }

        }
        // sendMessage("");
        return deposer;
    }
}
