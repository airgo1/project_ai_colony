package agence.planete;

public class TerrainAgent {
    // Type de terrain
    private char typeTerrain;
    private double valeur;
    private Boolean marqueur;

    // -1 pas de pipeline 0 pipeline cassé 1 pipeline construit
    private int pipeline;

    public TerrainAgent(char typeTerrain) {
        this.typeTerrain = typeTerrain;
        setValeurByTerrain();
        marqueur = false;
        pipeline = -1;

    }

    private void setValeurByTerrain() {
        if (typeTerrain == 'o' || typeTerrain == 'm') {
            valeur = 100;
        } else if (typeTerrain == 'e'){
            valeur = 200000;
        } else {
            valeur = 0;
        }
    }
    public char getTypeTerrain() {
        return typeTerrain;
    }

    public void setTypeTerrain(char typeTerrain) {
        if (this.typeTerrain != typeTerrain) {
            this.typeTerrain = typeTerrain;
            setValeurByTerrain();
            marqueur = false;
            if (pipeline == 1) pipeline = 0;
        }
    }

    public double getValeur() {
        return valeur;
    }

    public void setValeur(double valeur) {
        this.valeur = valeur;
        if (valeur == 0) {
            switch (typeTerrain) {
                case 'o':
                    typeTerrain = 's';
                    break;
                case 'm':
                    typeTerrain = 'p';
                    break;
            }
        }
    }

    public Boolean getMarqueur() {
        return marqueur;
    }

    public void setMarqueur(Boolean marqueur) {
        this.marqueur = marqueur;
    }

    public int getPipeline() {
        return pipeline;
    }

    public void setPipeline(int pipeline) {
        this.pipeline = pipeline;
    }
    @Override
    public String toString() {
        return "" + typeTerrain;
    }


}
