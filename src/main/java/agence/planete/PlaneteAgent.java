package agence.planete;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;
import net.sourceforge.jFuzzyLogic.rule.Variable;

import java.util.ArrayList;
import java.util.Random;

public class PlaneteAgent {
    // La planete est un Agent centralisé qui commande tous les agents de terrain

    private TerrainAgent[][] listeAgentTerrain;
    private int sizex;
    private int sizey;

    /**
     * Constructeur qui va récupérer les informations de la planete dans un fichier .data
     */
    public PlaneteAgent(int sizex, int sizey, ArrayList<TerrainAgent> listeAgentTerrain) {
        this.sizex = sizex;
        this.sizey = sizey;

        this.listeAgentTerrain = new TerrainAgent[sizex][sizey];
        int compteur = 0;
        for (int i = 0; i < sizex; i++) {
            for (int j = 0; j < sizey; j++) {
                this.listeAgentTerrain[i][j] = listeAgentTerrain.get(compteur);
                compteur++;
            }
        }
    }


    public TerrainAgent[][] getListeAgentTerrain() {
        return listeAgentTerrain;
    }

    public void setListeAgentTerrain(TerrainAgent[][] listeAgentTerrain) {
        this.listeAgentTerrain = listeAgentTerrain;
    }

    public int getSizex() {
        return sizex;
    }

    public void setSizex(int sizex) {
        this.sizex = sizex;
    }

    public int getSizey() {
        return sizey;
    }

    public void setSizey(int sizey) {
        this.sizey = sizey;
    }

    public ArrayList<String> getPositionTerrain(char c) {
        ArrayList<String> listeARenvoyer = new ArrayList<>();
        for (int i = 0; i < sizex; i++) {
            for (int j = 0; j < sizey; j++) {
                if (listeAgentTerrain[i][j].getTypeTerrain() == c) {
                    listeARenvoyer.add(i + "-" + j);
                }
            }
        }
        return listeARenvoyer;
    }

    private double fuzzyLoggic(double prelevement, double extraction) {
        // Load from 'FCL' file
        String fileName = "src/main/resources/fcl/planete.fcl";
        FIS fis = FIS.load(fileName, true);
        // Error while loading?
        if (fis == null) {
            System.err.println("Can't load file: '" + fileName + "'");
            return 0;
        }
        FunctionBlock functionBlock = new FunctionBlock(fis);

        // Show
        JFuzzyChart.get().chart(functionBlock);

        // Set inputs
        fis.setVariable("prelevement", prelevement);
        fis.setVariable("extraction", extraction);

        // Evaluate
        fis.evaluate();

        // Show output variable's chart
        Variable metamorphose = fis.getVariable("metamorphose");

        //JFuzzyChart.get().chart(metamorphose, metamorphose.getDefuzzifier(), true);
        return fis.getVariable("metamorphose").getDefuzzifier().defuzzify();
    }

    private char ruleOfTransformation(char typeTerrain) {
        int pourcentage = (int) (Math.random() * 101);
        switch (typeTerrain) {
            //Terrain infranchissable
            case 'x':
                // 30% de chance de devenir un desert
                if (pourcentage <= 30) {
                    return 'd';
                } else {
                    return 'x';
                }
            // Forêt à 100% de chance de se transformer
            case 'f':
                if (pourcentage <= 40) {
                    return 'g';
                } else if (pourcentage <= 60) {
                    return 's';
                } else if (pourcentage <= 90) {
                    return 'n';
                } else if (pourcentage <= 99) {
                    return 'd';
                } else {
                    System.out.println(pourcentage);

                    return 'x';
                }
            // Minerai
            case 'm':
                if (pourcentage <= 5) {
                    return 'p';
                } else {
                    return 'm';
                }
            // Prairie Sèche
            case 's':
                if (pourcentage <= 80) {
                    return 'd';
                } else if (pourcentage <= 99) {
                    return 'n';
                } else {
                    System.out.println(pourcentage);

                    return 'x';
                }
            // Prairie Normale
            case 'n':
                if (pourcentage <= 10) {
                    return 'd';
                } else if (pourcentage <= 70) {
                    return 's';
                } else {
                    return 'o';
                }
            // Prairie Grasse
            case 'g':
                if (pourcentage <= 5) {
                    return 'd';
                } else if (pourcentage <= 45) {
                    return 'n';
                } else if (pourcentage <= 75) {
                    return 's';
                } else {
                    return 'o';
                }
            // Desert
            case 'd':
                if (pourcentage <= 30) {
                    return 's';
                } else {
                    System.out.println(pourcentage);
                    return 'x';
                }
            // Nourriture
            case 'o':
                if (pourcentage <= 50) {
                    return 'g';
                } else if (pourcentage <= 80) {
                    return 'n';
                } else if (pourcentage <= 90) {
                    return 's';
                } else {
                    return 'f';
                }
            // Pierraille
            case 'p':
                if (pourcentage <= 2) {
                    return 'm';
                } else {
                    return 'p';
                }
        }
        return typeTerrain;
    }

    /**
     * @param prelevement
     * @param extraction
     */
    public void metamorphose(double prelevement, double extraction) {
        if (prelevement != 0 || extraction != 0) {
            System.out.println("La métamorphose de la planète est en cours...");
            double valeur = fuzzyLoggic(prelevement, extraction);
            // Il y a 21*21 case de terrain
            int posTerrainX, posTerrainY;
            Boolean valide = false;
            for (int i = 0; i < (sizey * sizex) * (valeur / 100); i++) {
                //do {
                posTerrainX = (int) (Math.random() * sizex);
                posTerrainY = (int) (Math.random() * sizey);

                listeAgentTerrain[posTerrainX][posTerrainY].setTypeTerrain(ruleOfTransformation(listeAgentTerrain[posTerrainX][posTerrainY].getTypeTerrain()));
                //} while (valide == false);
            }
            System.out.println("La planète s'est transformé...");
        }

    }
        /*
        Terrain initial Terrain métamorphosé Probabilité de réalisation (%)
        Forêt Prairie sèche 20
        Forêt Prairie normale 30
        Forêt Prairie grasse 40
        Forêt Désert 9
        Forêt Zone infranchissable 1
        Prairie sèche Désert 80
        Prairie sèche Nourriture 19
        Prairie sèche Zone infranchissable 1
        Prairie normale Désert 10
        Prairie normale Prairie sèche 60
        Prairie normale Nourriture 30
        Prairie grasse Désert 5
        Prairie grasse Prairie normale 40
        Prairie grasse Prairie sèche 30
        Prairie grasse Nourriture 25
        Désert Prairie sèche 30
        Désert Zone infranchissable 70
        Nourriture Prairie grasse 50
        Nourriture Prairie normale 30
        Nourriture Prairie sèche 10
        Nourriture Forêt 10
        Zone infranchissable Désert 30
        Pierraille Minerai 2
        Minerai Pierraille 5
        */

}



