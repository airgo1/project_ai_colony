package sample;

import gestionnaire.Gestionnaire;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/sample.fxml"));

        Parent root = (Parent) loader.load();

        primaryStage.setTitle("Colonisation FL - 669");
        primaryStage.setScene(new Scene(root, 1200, 800));
        primaryStage.getIcons().add(new Image("Assets/Base_Colonie.png"));
        primaryStage.show();

        Gestionnaire gest = (Gestionnaire) loader.getController();//new Gestionnaire();
        try {
            gest.launch();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
