package sample;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;
import net.sourceforge.jFuzzyLogic.rule.Variable;

public class TestFuzzyLogic {

    public static void main(String[] args) throws Exception {
        // Load from 'FCL' file
        String fileName = "src/main/resources/fcl/planete.fcl";
        FIS fis = FIS.load(fileName, true);
        // Error while loading?
        if (fis == null) {
            System.err.println("Can't load file: '" + fileName + "'");
            return;
        }
        FunctionBlock functionBlock = new FunctionBlock(fis);

        // Show
        JFuzzyChart.get().chart(functionBlock);

        // Set inputs
        fis.setVariable("prelevement", 50);
        fis.setVariable("extraction", 40);

        // Evaluate
        fis.evaluate();

        // Show output variable's chart
        Variable metamorphose = fis.getVariable("metamorphose");

        JFuzzyChart.get().chart(metamorphose, metamorphose.getDefuzzifier(), true);

        // Print ruleSet
        System.out.println(fis);
    }
}

