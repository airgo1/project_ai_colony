package gestionnaire;

import agence.planete.PlaneteAgent;
import agence.planete.TerrainAgent;

import java.util.Stack;

/**
 * L'environnement sert d'intermédiaire entre les capteurs et les informations de la planète
 * Il ne peut pas donner trop d'informations aux capteurs
 */
public class Environnement {

    private PlaneteAgent planete;
    private String TERRAIN = "xe";

    // Permet de récupérer sur le tour actuel le prélèvement et l'extraction effectué
    private int prelevementTour;
    private int extractionTour;

    public Environnement(PlaneteAgent planete) {
        this.planete = planete;
    }

    /**
     * Permet de récupérer une matrice autour de la case demandé
     * @param posx
     * @param posy
     * @param caseAutour
     * @return
     */
    private TerrainAgent[][] getMatrice(int posx, int posy, int caseAutour) {
        TerrainAgent[][] liste = planete.getListeAgentTerrain();
        int tailleMatrice = caseAutour * 2 + 1;
        TerrainAgent[][] listeReturn = new TerrainAgent[tailleMatrice][tailleMatrice];

        for (int i = 0; i < tailleMatrice; i++) {
            for (int j = 0; j < tailleMatrice; j++) {
                if (posx - caseAutour + i < 0 || posy - caseAutour + j < 0 || posx - caseAutour + i >= getSizeX() || posy - caseAutour + j >= getSizeY()) {
                    listeReturn[i][j] = new TerrainAgent('x');
                } else {
                    listeReturn[i][j] = liste[posx - caseAutour + i][posy - caseAutour + j];
                }
            }
        }
        return listeReturn;
    }

    /**
     * @param posx position du capteur en x
     * @param posy position du capteur en y
     * @return Boolean pour informer si c'est un obstacle ou non
     */
    public Boolean getObstable(int posx, int posy, int dirx, int diry, int portee) {
        // Direction 0 = NO 7=SE
        TerrainAgent[][] matrice = getMatrice(posx, posy, portee);

        if (TERRAIN.contains(Character.toString(matrice[dirx][diry].getTypeTerrain()))) {
            return true;
        }
        return false;
    }

    /**
     * Permet de savoir si l'élément sur la position est un obstacle ou non !
     * @param posx Position en x
     * @param posy Position en y
     * @return 1 si c'est un obstacle sinon 0
     */
    public int estObstacle(int posx, int posy) {
        if (TERRAIN.contains(Character.toString(planete.getListeAgentTerrain()[posx][posy].getTypeTerrain()))) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Permet de récupérer le type de la case courante (x,y)
     * @param posx Position en x
     * @param posy Position en y
     * @return
     */
    public char getCaseCourante(int posx, int posy) {
        return planete.getListeAgentTerrain()[posx][posy].getTypeTerrain();
    }

    /**
     * Informe si la case demandé possède un marqueur ou non
     * @param posx Position en x
     * @param posy Position en y
     * @return
     */
    public Boolean getMarqueur(int posx, int posy) {
        return planete.getListeAgentTerrain()[posx][posy].getMarqueur();
    }

    /**
     * Permet de rajouter un marqueur sur la position demandé
     * @param posx Position en x
     * @param posy Position en y
     */
    public void addMarqueur(int posx, int posy) {
        planete.getListeAgentTerrain()[posx][posy].setMarqueur(true);
    }

    /**
     * Permet de récolter de la nourriture sur la carte
     * @param posx Position en x
     * @param posy Position en y
     * @return 1 si récolte terminé 0 sinon
     */
    public int recolteNourriture(int posx, int posy) {
        planete.getListeAgentTerrain()[posx][posy].setValeur(planete.getListeAgentTerrain()[posx][posy].getValeur() - 10);
        if (planete.getListeAgentTerrain()[posx][posy].getValeur() == 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Permet de récupérer le minerai sur la carte
     * @param posx Position en x
     * @param posy Position en y
     * @return 1 si la valeur est tombé à 0 et 0 s'il reste du minerai
     */
    public int recolteMinerai(int posx, int posy) {
        planete.getListeAgentTerrain()[posx][posy].setValeur(planete.getListeAgentTerrain()[posx][posy].getValeur() - 10);
        if (planete.getListeAgentTerrain()[posx][posy].getValeur() == 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Récupére la taille en x de la planète
     * @return taille x
     */
    public int getSizeX() {
        return planete.getSizex();
    }

    /**
     * Récupére la taille en y de la planète
     * @return taille en y
     */
    public int getSizeY() {
        return planete.getSizey();
    }

    /**
     * Permet de réinitialiser le prélévement et l'extraction
     */
    public void resetPrelevExtract() {
        prelevementTour = 0;
        extractionTour = 0;
    }

    /**
     * Permet de construire un pipeline
     * @param posx Position en x
     * @param posy Position en y
     */
    public void construirePipeline(int posx, int posy) {
        planete.getListeAgentTerrain()[posx][posy].setPipeline(1);
    }

    /**
     * Permet d'avoir la valeur du pipeline sur la case demandé
     * @param posx Position en x
     * @param posy Position en y
     * @return état pipeline de la case demandé
     */
    public int verifPipeline(int posx, int posy) {
        return planete.getListeAgentTerrain()[posx][posy].getPipeline();
    }

}
