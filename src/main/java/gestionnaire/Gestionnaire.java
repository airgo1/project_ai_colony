package gestionnaire;

import agence.colonie.ColonieAgent;
import agence.planete.PlaneteAgent;
import agence.planete.TerrainAgent;
import gestionnaire.affichage.Carte;
import gestionnaire.affichage.InformationGestion;
import gestionnaire.utils.ReaderFile;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Joue le rôle de classe gérante permettant de gérer les tours des objets
 */
public class Gestionnaire {
    @FXML
    private ScrollPane scrollPane;

    @FXML
    private Label InfoPosition;

    @FXML private Label InfoNombreRobot;
    @FXML private Label InfoTaille;
    @FXML private Label InfoEau;
    @FXML private Label InfoMinerai;
    @FXML private Label InfoNourriture;


    private Carte carte;
    private InformationGestion info;
    private PlaneteAgent planete;
    private ColonieAgent colonie;
    private Environnement environnement;
    private ScheduledExecutorService scheduledExecutorService;

    /**
     * @throws Exception
     */
    public void launch() throws Exception
    {
        // Récupération des informations du terrain
        ReaderFile read = new ReaderFile();
        planete = new PlaneteAgent(21,21, read.lirePlaneteTerrainAgent("map_initial"));
        environnement = new Environnement(planete);
        ArrayList<String> positions = planete.getPositionTerrain('c');
        if (positions.size() > 1)
        {
            throw new Exception("Situation incorrecte => Plusieurs Colonies sur la map");
        }

        colonie = new ColonieAgent(Integer.parseInt(positions.get(0).split("-")[0]), Integer.parseInt(positions.get(0).split("-")[1]));
        scrollPane.setContent(carte = new Carte(planete, InfoPosition));

        info = new InformationGestion(InfoNombreRobot, InfoTaille, InfoEau, InfoMinerai, InfoNourriture);
        info.updateInfo(planete, colonie);

        Timeline timeline = new Timeline(new KeyFrame(
                Duration.millis(200),
                ae -> cycleDeVie()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.setAutoReverse(true);
        timeline.play();

    }

    /**
     * Lancement du cycle de vie de la colonie
     *
     */
    private void cycleDeVie() {
        environnement.resetPrelevExtract();
        colonie.lancementTour(environnement);
        planete.metamorphose(colonie.getNouveauPrelevement(), colonie.getNouvelleExtraction());
        afficheCarteTour(planete, colonie);
        info.updateInfo(planete, colonie);

    }


    public void afficheCarteTour(PlaneteAgent planete, ColonieAgent colonie) {
        carte.affiche(planete,colonie, InfoPosition);
    }

    public void afficheInformationTour (PlaneteAgent planete, ColonieAgent colons) {

        
    }

    @FXML
    public void onExit(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    public void onForceMeta(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Forcer la mutation de la planete");
        alert.setHeaderText("Attention : ");
        alert.setContentText("Ceci va totalement changer la planete");

        alert.showAndWait();
        planete.metamorphose(2,5);
        carte.affiche(planete,colonie, InfoPosition);

    }



    /**
     * Méthode pour afficher la planète sur la console
     * @param liste liste des agents terrain de la planète
     */
    private void affichage(TerrainAgent[][] liste) {
        for (int i = 0; i <planete.getSizex(); i++) {
            System.out.print("\n");
            for (int j = 0; j < planete.getSizey(); j++) {
                System.out.print(liste[i][j]);
            }
        }
    }
}
