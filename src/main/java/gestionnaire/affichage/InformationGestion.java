package gestionnaire.affichage;

import agence.colonie.ColonieAgent;
import agence.planete.PlaneteAgent;
import javafx.scene.control.Label;

public class InformationGestion {


    private Label InfoNombreRobot;
    private Label InfoTaille;
    private Label InfoEau;
    private Label InfoMinerai;
    private Label InfoNourriture;

    public InformationGestion(Label infoNombreRobot, Label infoTaille, Label infoEau, Label infoMinerai, Label infoNourriture) {
        InfoNombreRobot = infoNombreRobot;
        InfoTaille = infoTaille;
        InfoEau = infoEau;
        InfoMinerai = infoMinerai;
        InfoNourriture = infoNourriture;
    }

    public void updateInfo (PlaneteAgent planete, ColonieAgent colonie) {
        InfoTaille.setText(planete.getSizex() + " : " + planete.getSizey());

        //InfoEau.setText(String.valueOf(colonie.getRessourceEau()));
        //InfoMinerai.setText(String.valueOf(colonie.getRessourceMinerai()));
        //InfoNourriture.setText(String.valueOf(colonie.getRessourceNourriture()));

        //if(!colonie.getListeRobot().isEmpty())InfoNombreRobot.setText(String.valueOf(colonie.getListeRobot().size()));
    }
}
