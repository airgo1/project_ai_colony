package gestionnaire.affichage;

import agence.colonie.Agent;
import agence.colonie.ColonieAgent;
import agence.planete.PlaneteAgent;
import agence.planete.TerrainAgent;
import javafx.geometry.Dimension2D;
import javafx.scene.CacheHint;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.List;

public class Carte extends Group {

    private static int CaseTailleX = 124;
    private static int CaseTailleXpar2 = CaseTailleX / 2;
    private static int CaseTailleY = 72;
    private static int CaseTailleYpar2 = CaseTailleY / 2;

    private List<Integer> indexlist;
    private int index;

    private Dimension2D dimension;

    private Image eau = new Image("Assets/Eau.png",150, 150, true, true);
    private Image minerai = new Image("Assets/Minerai.png",150, 150, true, true);
    private Image pierraile = new Image("Assets/Pierraille.png",150, 150, true, true);
    private Image foret = new Image("Assets/Foret.png",150, 150, true, true);
    private Image prairieS = new Image("Assets/Prairie_Seche.png",150, 150, true, true);
    private Image prairieN = new Image("Assets/Prairie_Normale.png",150, 150, true, true);
    private Image prairieG = new Image("Assets/Prairie_Grasse.png",150, 150, true, true);
    private Image desert = new Image("Assets/Desert.png",150, 150, true, true);
    private Image nourriture = new Image("Assets/Nourriture.png",150, 150, true, true);
    private Image zoneInfranchissable = new Image("Assets/Zone_Infranchissable.png",150, 150, true, true);
    private Image BaseColonie = new Image("Assets/Base_Colonie.png",150, 150, true, true);

    private Image pipelane = new Image("Assets/Pipelane.png",150, 150, true, true);


    private Image robot = new Image("Assets/Robot.png",75,75,true,true);

    public  Carte (PlaneteAgent planete, Label InfoPosition) {
        this.setCache(true);
        this.setCacheHint(CacheHint.SPEED);

        dimension = new Dimension2D(planete.getSizex(),planete.getSizey());

        TerrainAgent[][] liste = planete.getListeAgentTerrain();

        for (int row = 0; row < dimension.getWidth(); row++) {
            for (int col = 0; col < dimension.getHeight(); col++) {

                ImageView tile = new ImageView();

                if ('e' == liste[col][row].getTypeTerrain()) tile.setImage(eau);
                if ('m' == liste[col][row].getTypeTerrain()) tile.setImage(minerai);
                if ('p' == liste[col][row].getTypeTerrain()) tile.setImage(pierraile);
                if ('f' == liste[col][row].getTypeTerrain()) tile.setImage(foret);
                if ('s' == liste[col][row].getTypeTerrain()) tile.setImage(prairieS);
                if ('n' == liste[col][row].getTypeTerrain()) tile.setImage(prairieN);
                if ('g' == liste[col][row].getTypeTerrain()) tile.setImage(prairieG);
                if ('d' == liste[col][row].getTypeTerrain()) tile.setImage(desert);
                if ('o' == liste[col][row].getTypeTerrain()) tile.setImage(nourriture);
                if ('x' == liste[col][row].getTypeTerrain()) tile.setImage(zoneInfranchissable);
                if ('c' == liste[col][row].getTypeTerrain()) tile.setImage(BaseColonie);

                tile.setX((col - row) * CaseTailleXpar2);
                tile.setY((col + row) * CaseTailleYpar2);

                String tileText = "Position : " + col + ":" + row;

                tile.setOnMouseEntered(event -> {
                    InfoPosition.setText(tileText);
                });

                this.getChildren().add(tile);

            }
        }
    }

    public void affiche(PlaneteAgent planete, ColonieAgent colons, Label InfoPosition) {

        if(this.getChildren().size()>1000) this.getChildren().remove(0, 500);


        TerrainAgent[][] liste = planete.getListeAgentTerrain();
        ArrayList<Agent> listerobot = colons.getListeRobot();

        for (int row = 0; row < dimension.getWidth(); row++) {
            for (int col = 0; col < dimension.getHeight(); col++) {

                ImageView tile = new ImageView();

                if ( 'e' == liste[col][row].getTypeTerrain() ) tile.setImage(eau);
                if ('m' == liste[col][row].getTypeTerrain()) tile.setImage(minerai);
                if ('p' == liste[col][row].getTypeTerrain()) tile.setImage(pierraile);
                if ('f' == liste[col][row].getTypeTerrain()) tile.setImage(foret);
                if ('s' == liste[col][row].getTypeTerrain()) tile.setImage(prairieS);
                if ('n' == liste[col][row].getTypeTerrain()) tile.setImage(prairieN);
                if ('g' == liste[col][row].getTypeTerrain()) tile.setImage(prairieG);
                if ('d' == liste[col][row].getTypeTerrain()) tile.setImage(desert);
                if ('o' == liste[col][row].getTypeTerrain()) tile.setImage(nourriture);
                if ('x' == liste[col][row].getTypeTerrain()) tile.setImage(zoneInfranchissable);
                if ('c' == liste[col][row].getTypeTerrain()) tile.setImage(BaseColonie);

                tile.setX((col - row) * CaseTailleXpar2);
                tile.setY((col + row) * CaseTailleYpar2);

                String tileText = "Position : " + col + ":" + row;

                this.getChildren().add(tile);

                if (liste[col][row].getPipeline() != -1) {
                    ImageView tilepipeplan = new ImageView(pipelane);

                    tilepipeplan.setX((col - row) * CaseTailleXpar2);
                    tilepipeplan.setY((col + row) * CaseTailleYpar2);

                    this.getChildren().add(tilepipeplan);

                    tileText = "Position : " + col + ":" + row;

                }

                String finalTileText = tileText;
                tile.setOnMouseEntered(event -> {
                    InfoPosition.setText(finalTileText);
                });
            }
        }

        DropShadow dropShadow2 = new DropShadow();
        dropShadow2.setOffsetX(3.0);
        dropShadow2.setOffsetY(7.0);
        dropShadow2.setRadius(10);

        for (int i=0; i<listerobot.size();i++) {
            ImageView tileRobot = new ImageView(robot);

            tileRobot.setX((listerobot.get(i).getPosx() - listerobot.get(i).getPosy()) * CaseTailleXpar2 + CaseTailleXpar2 - CaseTailleXpar2/2.5);
            tileRobot.setY((listerobot.get(i).getPosx() + listerobot.get(i).getPosy()) * CaseTailleYpar2 - CaseTailleYpar2 + CaseTailleYpar2/2);

            String tileText = "Position : " + listerobot.get(i).getPosx() + ":" + listerobot.get(i).getPosy();

            tileRobot.setOnMouseEntered(event -> {
                InfoPosition.setText(tileText);
            });

            tileRobot.setEffect(dropShadow2);

            this.getChildren().add(tileRobot);

        }
    }



}
