package gestionnaire.utils;

import agence.planete.TerrainAgent;
import gestionnaire.Environnement;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Astar {
    private ArrayList<CellAstar> ferme = new ArrayList<>();
    private ArrayList<CellAstar> ouverte = new ArrayList<>();
    private Set<CellAstar> path = new HashSet<>();

    boolean go = false;
    private int[][] pos;

    // les lignes 0 et les colonnes 0 sont ignorées dans le calcul du chemain
    // elles nous permettent seulement d'avoir un tableau de recherche commençant à 1 au lieu de 0;
    /*static int[][] pos = {
            {5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5},//0 //ceci est un exemple d'utilsation
            {5, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0},//1
            {5, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0},//2
            {5, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0},//3
            {5, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1},//4
            {5, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0},//5
            {5, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0},//6
            {5, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0},//7
            {5, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1},//8
            {5, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0},//9
            {5, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0}};//10
    ///////////// 0 1 2 3 4 5 6 7 8 9 10
    */

    private CellAstar debut;
    private CellAstar fin;


    public Astar() {

    }

    public Astar(Environnement env, int posx, int posy, int basePosX, int basePosY) {
        pos = new int[env.getSizeX()][env.getSizeY()];
        for (int i = 0; i < env.getSizeX(); i++) {
            for (int j = 0; j < env.getSizeY(); j++) {

                pos[i][j] = env.estObstacle(i, j);
            }
        }
        debut = new CellAstar(posx, posy);
        fin = new CellAstar(basePosX, basePosY);
        ajoutAdjacentAOuverte(debut);

    }

    public Astar(CellAstar debut, CellAstar fin) {
        if (pos[debut.getX()][debut.getY()] != 1 && pos[fin.getX()][fin.getY()] != 1) {
            this.debut = debut;
            this.fin = fin;
            ajoutAdjacentAOuverte(debut);
        } else System.out.println("Vous êtes sur un mur !!!");
    }

    public Astar(CellAstar debut, CellAstar fin, int[][] pos) {
        if (pos[debut.getX()][debut.getY()] != 1 && pos[fin.getX()][fin.getY()] != 1) {
            this.pos = pos;
            this.debut = debut;
            this.fin = fin;
            ajoutAdjacentAOuverte(debut);
        } else System.out.println("Vous êtes sur un mur !!!");
    }

    public void ajoutAdjacentAOuverte(CellAstar debut) {
        int xc, yc;

        CellAstar courante = debut;
        CellAstar memoire = null;

        while (!isInList(fin, ferme)) {

            if (!isInList(courante, ferme)) {
                ferme.add(courante);

                xc = courante.getX();
                yc = courante.getY();

                // Haut
                if ((yc >= 2) && (pos[xc][yc - 1] != 1))
                    ajoutOuverte(courante, (new CellAstar(xc, yc - 1)));
                // Bas
                if ((yc < pos.length - 1) && (pos[xc][yc + 1] != 1))
                    ajoutOuverte(courante, (new CellAstar(xc, yc + 1)));
                if (xc < pos.length - 1) {
                    //droite
                    if ((pos[xc + 1][yc] != 1)) ajoutOuverte(courante, (new CellAstar(xc + 1, yc)));
                    // droite haut
                    if ((yc >= 2) && (pos[xc + 1][yc - 1] != 1) && ((pos[xc][yc - 1] == 1) || (pos[xc + 1][yc] == 1)))
                        ajoutOuverte(courante, (new CellAstar(xc + 1, yc - 1)));
                    if ((yc < pos.length - 1) && (pos[xc + 1][yc + 1] != 1) && ((pos[xc + 1][yc] == 1) || (pos[xc][yc + 1] == 1)))
                        ajoutOuverte(courante, (new CellAstar(xc + 1, yc + 1)));
                }
                if (xc >= 2) {
                    if ((pos[xc - 1][yc] != 1))
                        ajoutOuverte(courante, (new CellAstar(xc - 1, yc)));
                    if ((yc >= 2) && (pos[xc - 1][yc - 1] != 1) && ((pos[xc - 1][yc] == 1) || (pos[xc][yc - 1] == 1)))
                        ajoutOuverte(courante, (new CellAstar(xc - 1, yc - 1)));
                    if ((yc < pos.length - 1) && (pos[xc - 1][yc + 1] != 1) && ((pos[xc - 1][yc] == 1) || (pos[xc][yc + 1] == 1)))
                        ajoutOuverte(courante, (new CellAstar(xc - 1, yc + 1)));
                }
                memoire = courante;
            }
            ouverte.remove(courante);
            if (ouverte.isEmpty()) {
                if (Math.abs(memoire.getX() - fin.getX()) >= 1 && Math.abs(memoire.getY() - fin.getY()) >= 1) {
                    System.out.println("Il n'y a pas de chemin entre ces deux point !!!");
                    break;
                } else break;
            }
            courante = getMinF();
        }

        fin.setParent(memoire);
        getParentPath();
        //dessine();
        dessineResult();
    }

    public void afficheList(ArrayList<CellAstar> list) {
        Iterator<CellAstar> ito = list.iterator();
        CellAstar test;
        while (ito.hasNext()) {
            test = ito.next();
            System.out.println(test.toString() + "-->:" + " X= " + test.getX() + " Y= " + test.getY() + " F= " + test.getF());
        }

    }

    public boolean isInList(CellAstar courante, ArrayList<CellAstar> list) {
        Iterator ito = list.iterator();
        while (ito.hasNext()) {
            if (ito.next().equals(courante)) return true;
        }
        return false;
    }

    public void ajoutOuverte(CellAstar courante, CellAstar adjacente) {
        int g = courante.getG() + ((adjacente.getX() == courante.getX() || adjacente.getY() == courante.getY()) ? 10 : 15);
        int h = (Math.abs(adjacente.getX() - fin.getX()) + Math.abs(adjacente.getY() - fin.getY()));
        int f = g + h;
        if (isInList(adjacente, ouverte)) {
            if (adjacente.getF() > f) {
                adjacente.setG(g);
                adjacente.setF(f);
                adjacente.setParent(courante);
            }
        } else if (!isInList(adjacente, ferme)) {
            adjacente.setG(g);
            adjacente.setH(h);
            adjacente.setF(f);
            adjacente.setParent(courante);
            ouverte.add(adjacente);
        }

    }

    public int[][] getPos() {
        return pos;
    }

    public void setPos(int[][] pos) {
        this.pos = pos;
    }

    public Set<CellAstar> getPath() {
        while (!go) {
            getPath();
        }
        return path;
    }

    public int getPathSize() {
        while (!go) {
            getPath();
        }
        return path.size();
    }

    public void setPath(Set<CellAstar> path) {
        this.path = path;
    }

    CellAstar getMinF() {
        CellAstar min = null;
        min = ouverte.get(0);
        Iterator<CellAstar> fIt = ouverte.iterator();

        while (fIt.hasNext()) {
            min = compareF(min, fIt.next());
        }
        return min;


    }

    void getParentPath() {

        CellAstar curr = this.fin;
        while (!curr.equals(debut)) {
            path.add(curr);
            curr = curr.getParent();
        }
        //path.add(debut);
        this.go = true;
    }

    CellAstar compareF(CellAstar cF1, CellAstar cF2) {
        if (cF1.getF() < cF2.getF()) return cF1;
        return cF2;
    }

    //********************************Test************************************************//

    void dessine() {

        for (int pl = 1; pl < pos.length; pl++) {
            for (int c = 1; c < pos[pl].length; c++) {
                System.out.print(pos[pl][c] + " ");
            }
            System.out.println();
        }
    }

    void dessineResult() {
        //System.out.println("*****************");
        Iterator<CellAstar> itFerme = path.iterator();
        CellAstar fil = null;
        while (itFerme.hasNext()) {
            fil = itFerme.next();
            pos[fil.getX()][fil.getY()] = 8;

        }

        //System.out.println("*****************");
        /*for (int pl = 1; pl < pos.length; pl++) {
            for (int c = 1; c < pos.length; c++) {
                System.out.print(pos[pl][c] + " ");
            }
            System.out.println();
        }
        */
    }


    public static void main(String[] args) {
        CellAstar debut = new CellAstar(6, 9);
        CellAstar fin = new CellAstar(1, 1);
        Astar recherche = new Astar(debut, fin);

    }

}
