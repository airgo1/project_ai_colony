package gestionnaire.utils;

import agence.planete.TerrainAgent;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * @Author Cédric & nicolas
 * <p>
 * Classe pour lire des informations dans un fichier
 */
public class ReaderFile {
    public ReaderFile() {
    }

    public ArrayList<TerrainAgent> lirePlaneteTerrainAgent(String nomFichier) {
        ArrayList<TerrainAgent> listeAgent = new ArrayList<>();
        String nom = "/Map/" + nomFichier + ".data";
        try {
            BufferedReader buff = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(nom)));
            //InputStream flux = new FileInputStream(nom);
            //InputStreamReader lecture = new InputStreamReader(flux);
            //BufferedReader buff = new BufferedReader(lecture);

            String ligne;
            while ((ligne = buff.readLine()) != null) {
                char[] liste = ligne.toCharArray();
                for (char e : liste) {
                    listeAgent.add(new TerrainAgent(e));
                }
            }
            buff.close();

        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return listeAgent;
    }

}
