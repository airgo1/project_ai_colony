package gestionnaire;

import java.util.HashMap;
import java.util.Map;

public class InfoGame {
    public static Map<Character, String> getTypeTerrain() {
        Map<Character, String> listeTypeTerrain = new HashMap<>();

        listeTypeTerrain.put('e', "Eau");
        listeTypeTerrain.put('m', "Minerai");
        listeTypeTerrain.put('p', "Pierraille");
        listeTypeTerrain.put('f', "Foret");
        listeTypeTerrain.put('s', "Prairie seche");
        listeTypeTerrain.put('n', "Prairie normale");
        listeTypeTerrain.put('g', "Prairie grasse");
        listeTypeTerrain.put('d', "Desert");
        listeTypeTerrain.put('o', "Nourriture");
        listeTypeTerrain.put('x', "Zone infranchissable");
        listeTypeTerrain.put('c', "Base de la colonie");

        return listeTypeTerrain;
    }

    public static Map<String, String> getOrientation() {
        Map<String, String> liste = new HashMap<>();

        //N
        liste.put("0", "0-1");
        //NE
        liste.put("1", "0-2");
        //E
        liste.put("2", "1-2");
        //SE
        liste.put("3", "2-2");
        //S
        liste.put("4", "2-1");
        //SW
        liste.put("5", "2-0");
        //W
        liste.put("6", "1-0");
        //NW
        liste.put("7", "0-0");

        return liste;
    }

}
